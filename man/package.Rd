\name{BSgenome.Stuberosum.SGSC.v11}
\docType{package}

\alias{BSgenome.Stuberosum.SGSC.v11-package}
\alias{BSgenome.Stuberosum.SGSC.v11}
\alias{StubSolyntus}

\title{Full Genome Sequence for SOLYNTUS}

\description{
  In view of the draft genome sequence of DM which still had much room for improvement it was decided by Solynta and Plant Breeding, Wageningen University & Research jointly to sequence one of the more homozygous selections of the program of Solynta. This unique genotype, called Solyntus is the first potato (Solanum tuberosum) genotype, that is highly homozygous, relatively vigorous and self-compatible. Solyntus was sequenced using Oxford Nanopore reads supplemented with Illumina reads to generate an improved reference genome for the cultivated potato.
}

\details{
Please cite also the original publication of the genome:

\href{https://www.g3journal.org/content/10/10/3489.abstract}{Solyntus, the New Highly Contiguous Reference Genome for Potato (Solanum tuberosum)}

\preformatted{

@article{van2020solyntus,
  title={Solyntus, the New Highly Contiguous Reference Genome for Potato (Solanum tuberosum)},
  author={van Lieshout, Natascha and 
          van der Burgt, Ate and 
          de Vries, Michiel E and 
          Ter Maat, Menno and 
          Eickholt, David and 
          Esselink, Danny and 
          van Kaauwen, Martijn PW and 
          Kodde, Linda P and Visser, 
          Richard GF and 
          Lindhout, Pim and 
          Finkers, Richard},
  journal={G3: Genes, Genomes, Genetics},
  volume={10},
  number={10},
  pages={3489--3495},
  year={2020},
  publisher={G3: Genes, Genomes, Genetics}
}
}
}

\note{
  This BSgenome data package was made from the following source data files:
  \preformatted{
FASTA.tar.gz from https://www.plantbreeding.wur.nl/Solyntus/#download
  }

  See \code{?\link[BSgenome]{BSgenomeForge}} and the BSgenomeForge
  vignette (\code{vignette("BSgenomeForge")}) in the \pkg{BSgenome}
  software package for how to make a BSgenome data package.
}

\author{Reinhard Simon}

\seealso{
  \itemize{
    \item \link[BSgenome]{BSgenome} objects and the
          \code{\link[BSgenome]{available.genomes}} function
          in the \pkg{BSgenome} software package.
    \item \link[Biostrings]{DNAString} objects in the \pkg{Biostrings}
          package.
    \item The BSgenomeForge vignette (\code{vignette("BSgenomeForge")})
          in the \pkg{BSgenome} software package for how to make a BSgenome
          data package.
  }
}

\examples{
BSgenome.Stuberosum.SGSC.v11
genome <- BSgenome.Stuberosum.SGSC.v11
head(seqlengths(genome))


## ---------------------------------------------------------------------
## Genome-wide motif searching
## ---------------------------------------------------------------------
## See the GenomeSearching vignette in the BSgenome software
## package for some examples of genome-wide motif searching using
## Biostrings and the BSgenome data packages:
if (interactive())
    vignette("GenomeSearching", package="BSgenome")
}

\keyword{package}
\keyword{data}
