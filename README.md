
# BSgenome.Stuberosum.SGSC.v11

<!-- badges: start -->
<!-- badges: end -->

In view of the draft genome sequence of DM which still had much room for improvement it was decided by Solynta and Plant Breeding, Wageningen University & Research jointly to sequence one of the more homozygous selections of the program of Solynta. This unique genotype, called Solyntus is the first potato (Solanum tuberosum) genotype, that is highly homozygous, relatively vigorous and self-compatible. Solyntus was sequenced using Oxford Nanopore reads supplemented with Illumina reads to generate an improved reference genome for the cultivated potato.

## Installation

You can install the released version of BSgenome.Stuberosum.SGSC.v11 from [CRAN](https://CRAN.R-project.org) with:

``` r
install.packages("remotes")
library(remotes)
install_gitlab("kteam/BSgenome.Stuberosum.SGSC.v11",
                host="https://git.wur.nl/")

```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(BSgenome.Stuberosum.SGSC.v11)
genome <- BSgenome.Stuberosum.SGSC.v11
genome
head(seqlengths(genome))
```

## References

Please cite also the original publication of the genome:

```
@article{van2020solyntus,
  title={Solyntus, the New Highly Contiguous Reference Genome for Potato (Solanum tuberosum)},
  author={van Lieshout, Natascha and 
          van der Burgt, Ate and 
          de Vries, Michiel E and 
          Ter Maat, Menno and 
          Eickholt, David and 
          Esselink, Danny and 
          van Kaauwen, Martijn PW and 
          Kodde, Linda P and 
          Visser, Richard GF and 
          Lindhout, Pim and 
          Finkers, Richard},
  journal={G3: Genes, Genomes, Genetics},
  volume={10},
  number={10},
  pages={3489--3495},
  year={2020},
  publisher={G3: Genes, Genomes, Genetics}
}
```